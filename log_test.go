package logger

import (
	"bytes"
	"io"
	"testing"
)

var testLog *Log
var bufInfo, bufDebug, bufWarn, bufError bytes.Buffer

func init() {
	testLog = New(&bufInfo, &bufDebug, &bufWarn, &bufError)
}

func TestInfo(t *testing.T) {
	testTag := "testInfoTag"
	testVal := "my value"
	testLog.Info(testTag, testVal)
	got, err := bufInfo.ReadString('\n')
	if err != nil {
		t.Error(err)
	}
	bufInfo.Reset()
	t.Log(got)
	testLog.Info(testVal)
	got2, err := bufInfo.ReadString('\n')
	if err != nil && err != io.EOF {
		t.Error(err)
	}
	t.Log(got2)
}

func TestDebug(t *testing.T) {
	testTag := "testInfoTag"
	testVal := "my value"
	testLog.Debug(testTag, testVal)
	got, err := bufDebug.ReadString('\n')
	if err != nil {
		t.Error(err)
	}
	bufDebug.Reset()
	t.Log(got)
	testLog.Debug(testVal)
	got2, err := bufDebug.ReadString('\n')
	if err != nil && err != io.EOF {
		t.Error(err)
	}
	t.Log(got2)
}
func TestWarn(t *testing.T) {
	testTag := "testInfoTag"
	testVal := "my value"
	testLog.Warn(testTag, testVal)
	got, err := bufWarn.ReadString('\n')
	if err != nil {
		t.Error(err)
	}
	bufWarn.Reset()
	t.Log(got)
	testLog.Warn(testVal)
	got2, err := bufWarn.ReadString('\n')
	if err != nil && err != io.EOF {
		t.Error(err)
	}
	t.Log(got2)
}

func TestError(t *testing.T) {
	testTag := "testInfoTag"
	testVal := "my value"
	testLog.Error(testTag, testVal)
	got, err := bufError.ReadString('\n')
	if err != nil {
		t.Error(err)
	}
	bufError.Reset()
	t.Log(got)
	testLog.Error(testVal)
	got2, err := bufError.ReadString('\n')
	if err != nil && err != io.EOF {
		t.Error(err)
	}
	t.Log(got2)
}
