package logger

import (
	"fmt"
	"io"
	"time"
)

// Log struct
type Log struct {
	infoW, debugW, warnW, errorW io.Writer
}

// New returns a new Log instance
func New(infoW, debugW, warnW, errorW io.Writer) *Log {
	return &Log{infoW, debugW, warnW, errorW}
}

// Info level logger
func (log *Log) Info(tag string, args ...interface{}) {
	if tag == "" || len(args) == 0 {
		log.infoW.Write([]byte(fmt.Sprintf("LOGGING FAILED:\t tag='/%s' args=%+v\n", tag, args)))
		return
	}
	msg, ok := args[0].(string)
	if ok {
		args = args[1:]
	} else {
		msg = "%+v"
	}
	log.infoW.Write([]byte(fmt.Sprintf("%s\t%s:\t[/%s]\t%s\n", time.Now().Format(time.RFC3339), "INFO", tag, fmt.Sprintf(msg, args...))))
}

// Debug level logger
func (log *Log) Debug(tag string, args ...interface{}) {
	if tag == "" || len(args) == 0 {
		log.debugW.Write([]byte(fmt.Sprintf("LOGGING FAILED:\t tag='/%s' args=%+v\n", tag, args)))
		return
	}
	msg, ok := args[0].(string)
	if ok {
		args = args[1:]
	} else {
		msg = "%+v"
	}
	log.debugW.Write([]byte(fmt.Sprintf("%s\t%s:\t[/%s]\t%s\n", time.Now().Format(time.RFC3339), "DEBUG", tag, fmt.Sprintf(msg, args...))))
}

// Warn level logger
func (log *Log) Warn(tag string, args ...interface{}) {
	if tag == "" || len(args) == 0 {
		log.warnW.Write([]byte(fmt.Sprintf("LOGGING FAILED:\t tag='/%s' args=%+v\n", tag, args)))
		return
	}
	msg, ok := args[0].(string)
	if ok {
		args = args[1:]
	} else {
		msg = "%+v"
	}
	log.warnW.Write([]byte(fmt.Sprintf("%s\t%s:\t[/%s]\t%s\n", time.Now().Format(time.RFC3339), "WARN", tag, fmt.Sprintf(msg, args...))))
}

// Error level logger
func (log *Log) Error(tag string, args ...interface{}) {
	if tag == "" || len(args) == 0 {
		log.errorW.Write([]byte(fmt.Sprintf("LOGGING FAILED:\t tag='/%s' args=%+v\n", tag, args)))
		return
	}
	msg, ok := args[0].(string)
	if ok {
		args = args[1:]
	} else {
		msg = "%+v"
	}
	log.errorW.Write([]byte(fmt.Sprintf("%s\t%s:\t[/%s]\t%s\n", time.Now().Format(time.RFC3339), "ERROR", tag, fmt.Sprintf(msg, args...))))
}
